package com.mose.service;


import org.springframework.stereotype.Component;
/**
 * Hystrix 服务降级
 * 解耦回退逻辑
 */
@Component
public class DeptHystrixFallBackService implements DeptHystrixService {
    @Override
    public String deptInfo_Ok(Integer id) {
        return "--------------------系统提醒您，系统繁忙，请稍后重试！（解耦回退方法触发1）-----------------------";
    }
    @Override
    public String deptInfo_Timeout(Integer id) {
        return "--------------------系统提醒您，系统繁忙，请稍后重试！（解耦回退方法触发2）-----------------------";
    }
}
