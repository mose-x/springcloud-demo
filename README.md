# springcloud-demo

#### 介绍
springcloud-demo

1. 配置hosts文件,方便在eureka中观察
127.0.0.1  eureka7001.com
127.0.0.1  eureka7002.com
127.0.0.1  eureka7003.com

2. 7001-7003为eureka服务发现集群,参考就行,测试代码中只配置一个
3. 8001-8003为数据提供服务,生产者,全部运行的时候,方便观察集群的负载
4. gateway 9527只有一个,也可定义多个
5. config-center,config service服务,3344,只有一个,也可定义多个
6. config-client,config客户端,3355-3366,多个一起运行
7. 其他的consumer,单独启动,不要一次启动完
8. 数据库,rabbit,git配置仓库, 也需要搭建