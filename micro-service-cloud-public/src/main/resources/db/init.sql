DROP DATABASE IF EXISTS cloud1;

CREATE DATABASE cloud1 CHARACTER SET UTF8;

USE cloud1;

DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`
(
    `dept_no`   int NOT NULL AUTO_INCREMENT,
    `dept_name` varchar(255) DEFAULT NULL,
    `db_source` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`dept_no`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `dept`
VALUES ('1', '开发部', DATABASE());
INSERT INTO `dept`
VALUES ('2', '人事部', DATABASE());
INSERT INTO `dept`
VALUES ('3', '财务部', DATABASE());
INSERT INTO `dept`
VALUES ('4', '市场部', DATABASE());
INSERT INTO `dept`
VALUES ('5', '运维部', DATABASE());

#############################################################################################
DROP DATABASE IF EXISTS cloud2;

CREATE DATABASE cloud2 CHARACTER SET UTF8;

USE cloud2;

DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`
(
    `dept_no`   int NOT NULL AUTO_INCREMENT,
    `dept_name` varchar(255) DEFAULT NULL,
    `db_source` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`dept_no`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `dept`
VALUES ('1', '开发部', DATABASE());
INSERT INTO `dept`
VALUES ('2', '人事部', DATABASE());
INSERT INTO `dept`
VALUES ('3', '财务部', DATABASE());
INSERT INTO `dept`
VALUES ('4', '市场部', DATABASE());
INSERT INTO `dept`
VALUES ('5', '运维部', DATABASE());

#############################################################################################
DROP DATABASE IF EXISTS cloud3;

CREATE DATABASE cloud3 CHARACTER SET UTF8;

USE cloud3;

DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`
(
    `dept_no`   int NOT NULL AUTO_INCREMENT,
    `dept_name` varchar(255) DEFAULT NULL,
    `db_source` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`dept_no`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `dept`
VALUES ('1', '开发部', DATABASE());
INSERT INTO `dept`
VALUES ('2', '人事部', DATABASE());
INSERT INTO `dept`
VALUES ('3', '财务部', DATABASE());
INSERT INTO `dept`
VALUES ('4', '市场部', DATABASE());
INSERT INTO `dept`
VALUES ('5', '运维部', DATABASE());